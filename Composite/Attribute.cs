﻿using System.Collections.Generic;

namespace Composite
{
    public class Attribute
    {
        public List<Attribute> AddOns { get; } = new List<Attribute>();

        public string Name { get; }
        public int BaseValue { get; }
        public int Multiplier { get; }

        public Attribute(string name, int value, int multiplier)
        {
            Name = name;
            BaseValue = value;
            Multiplier = multiplier;
        }

        public void Upgrade(Attribute addOn)
        {
            AddOns.Add(addOn);
        }

        public void Downgrade(Attribute addOn)
        {
            AddOns.Remove(addOn);
        }
    }
}
