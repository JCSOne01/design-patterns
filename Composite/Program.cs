﻿using System;

namespace Composite
{
    internal class Program
    {
        private Attribute _attribute;
        private static void Main(string[] args)
        {
            var intelligence = new Attribute("Inteligencia", 10, 1);
            var cunning = new Attribute("Astucia", 10, 2);
            cunning.Upgrade(new Attribute("Astucia nivel 2", 20, 2));
            cunning.Upgrade(new Attribute("Astucia nivel 3", 20, 2));
            cunning.Upgrade(new Attribute("Astucia nivel 4", 20, 2));
            var problemSolving = new Attribute("Solucion de problemas", 20, 3);
            problemSolving.Upgrade(new Attribute("Solucion de problemas nivel 1", 40, 2));
            intelligence.Upgrade(cunning);
            intelligence.Upgrade(problemSolving);

            Console.WriteLine("Los niveles de inteligencia son");
            intelligence.AddOns.ForEach(ao =>
            {
                Console.WriteLine($"{ao.Name}:");
                ao.AddOns.ForEach(sao =>
                {
                    Console.WriteLine(sao.Name);
                });
            });

            Console.ReadLine();
        }
    }
}
