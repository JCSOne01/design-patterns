﻿using System;

namespace Adapter
{
    public class Ship : IVehicle
    {
        public void Fire()
        {
            Console.WriteLine("La nave ha disparado");
        }

        public void Fly(Direction direction)
        {
            switch (direction)
            {
                case Direction.Down:
                    Console.WriteLine("La nave está descendiendo");
                    break;
                case Direction.Up:
                    Console.WriteLine("La nave está ascendiendo");
                    break;
                case Direction.Left:
                    Console.WriteLine("La nave está retrocediendo");
                    break;
                case Direction.Right:
                    Console.WriteLine("La nave está avanzando");
                    break;
                default:
                    Console.WriteLine("No se ha podido ejecutar el movimiento");
                    break;
            }
        }
    }
}
