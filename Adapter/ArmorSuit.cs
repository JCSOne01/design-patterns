﻿using System;

namespace Adapter
{
    public class ArmorSuit
    {
        public void ThrowPunch()
        {
            Console.WriteLine("La armadura a dado un puñetazo");
        }

        public void Move(Direction direction)
        {
            switch (direction)
            {
                case Direction.Down:
                    Console.WriteLine("La armadura se ha agachado");
                    break;
                case Direction.Up:
                    Console.WriteLine("La armadura ha saltado");
                    break;
                case Direction.Left:
                    Console.WriteLine("La armadura está caminando a la izquierda");
                    break;
                case Direction.Right:
                    Console.WriteLine("La armadura está caminando a la derecha");
                    break;
                default:
                    Console.WriteLine("No se ha podido ejecutar el movimiento");
                    break;
            }
        }
    }
}
