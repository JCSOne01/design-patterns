﻿namespace Adapter
{
    public enum Direction
    {
        Down,
        Left,
        Right,
        Up
    }
}
