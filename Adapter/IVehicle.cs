﻿namespace Adapter
{
    public interface IVehicle
    {
        void Fire();
        void Fly(Direction direction);
    }
}
