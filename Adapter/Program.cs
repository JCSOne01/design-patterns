﻿using System;

namespace Adapter
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Implementación inicial de vehiculo (nave)");
            IVehicle ship = new Ship();
            ship.Fire();
            ship.Fly(Direction.Left);
            ship.Fly(Direction.Down);

            Console.WriteLine();
            Console.WriteLine("Nuevo objeto (Armadura)");
            var armorSuit = new ArmorSuit();
            armorSuit.ThrowPunch();
            armorSuit.Move(Direction.Right);
            armorSuit.Move(Direction.Up);

            Console.WriteLine();
            Console.WriteLine("Armadura adaptada para funcionar como vehiculo");
            IVehicle armorSuitAdapter = new ArmorSuitAdapter(armorSuit);
            armorSuitAdapter.Fire();
            armorSuitAdapter.Fly(Direction.Left);
            armorSuitAdapter.Fly(Direction.Down);

            Console.ReadLine();
        }
    }
}
