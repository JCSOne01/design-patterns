﻿namespace Adapter
{
    public class ArmorSuitAdapter : IVehicle
    {
        private readonly ArmorSuit _armorSuit;

        public ArmorSuitAdapter(ArmorSuit armorSuit)
        {
            _armorSuit = armorSuit;
        }

        public void Fire()
        {
            _armorSuit.ThrowPunch();
        }

        public void Fly(Direction direction)
        {
            _armorSuit.Move(direction);
        }
    }
}
