﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Proxy
{
    public interface IDoor
    {
        void Open();
        void Close();
    }
}
