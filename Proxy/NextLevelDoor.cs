﻿using System;

namespace Proxy
{
    public class NextLevelDoor : IDoor
    {
        private IDoor _door;
        private bool _isOpen = false;

        public void Open()
        {
            _isOpen = true;
            Console.WriteLine("Opening the door to advance to next level");
        }

        public void Close()
        {
            if (!_isOpen)
            {
                Console.WriteLine("The door isn't open");
                return;
            }

            Console.WriteLine("Closing the door, previous level is now unavailable");
            _isOpen = false;
        }
    }
}
