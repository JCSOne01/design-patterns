﻿using System;

namespace Proxy
{
    internal class Program
    {
        private static BlockedDoor _blockedDoor;
        private static Player _player;

        private static void Main(string[] args)
        {
            var door = new NextLevelDoor();

            Console.WriteLine("Interacting with a basic door");
            Console.WriteLine();

            door.Open();
            door.Close();

            Console.WriteLine();
            Console.WriteLine("Adding requirements to open the door");
            Console.WriteLine();

            _blockedDoor = new BlockedDoor(door);
            _player = new Player();
            _blockedDoor.Open(_player);
            _blockedDoor.Close();

            Console.WriteLine();
            Console.WriteLine("Let's give the doors key to the player");
            Console.WriteLine();

            _player.HasKey = true;
            _blockedDoor.Open(_player);
            _blockedDoor.Close();

            Console.ReadLine();
        }
    }
}
