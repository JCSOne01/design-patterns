﻿using System;

namespace Proxy
{
    internal class BlockedDoor
    {
        private readonly IDoor _door;

        public BlockedDoor(IDoor door)
        {
            _door = door;
        }

        public void Open(Player player)
        {
            if (player.HasKey)
            {
                _door.Open();
                return;
            }
            Console.WriteLine("You don't have the key");
        }

        public void Close()
        {
            _door.Close();
        }
    }
}
