﻿using System;

namespace Prototype
{
    public class FinalBoss : Enemy
    {
        public override Enemy Clone()
        {
            return (Enemy)MemberwiseClone();
        }

        public override void Draw()
        {
            Console.WriteLine("Dibujando un jefe final");
        }

        public override void Update()
        {
            Console.WriteLine("Actualizando un jefe final");
        }
    }
}
