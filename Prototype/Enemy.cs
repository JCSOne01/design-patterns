﻿namespace Prototype
{
    public abstract class Enemy
    {
        public abstract Enemy Clone();
        public abstract void Draw();
        public abstract void Update();
    }
}
