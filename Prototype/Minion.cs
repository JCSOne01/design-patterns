﻿using System;

namespace Prototype
{
    public class Minion : Enemy
    {
        public override Enemy Clone()
        {
            return (Enemy)MemberwiseClone();
        }

        public override void Draw()
        {
            Console.WriteLine("Dibujando un minion");
        }

        public override void Update()
        {
            Console.WriteLine("Actualizando un minion");
        }
    }
}
