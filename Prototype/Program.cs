﻿using System;

namespace Prototype
{
    class Program
    {
        private EnemiesCache EnemiesCache;
        static void Main(string[] args)
        {
            var minion = EnemiesCache.Instance.GetEnemy("minion");
            minion.Update();
            minion.Draw();

            var miniBoss = EnemiesCache.Instance.GetEnemy("miniBoss");
            miniBoss.Update();
            miniBoss.Draw();

            var finalBoss = EnemiesCache.Instance.GetEnemy("finalBoss");
            finalBoss.Update();
            finalBoss.Draw();

            Console.ReadLine();
        }
    }
}
