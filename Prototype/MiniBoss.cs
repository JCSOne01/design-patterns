﻿using System;

namespace Prototype
{
    public class MiniBoss : Enemy
    {
        public override Enemy Clone()
        {
            return (Enemy)MemberwiseClone();
        }

        public override void Draw()
        {
            Console.WriteLine("Dibujando un mini jefe");
        }

        public override void Update()
        {
            Console.WriteLine("Actualizando un mini jefe");
        }
    }
}
