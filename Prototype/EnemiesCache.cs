﻿using System.Collections.Generic;

namespace Prototype
{
    public class EnemiesCache
    {
        private static readonly Dictionary<string, Enemy> _enemiesCache = new Dictionary<string, Enemy>();

        private static EnemiesCache _instance;

        public static EnemiesCache Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new EnemiesCache();
                }

                return _instance;
            }
        }

        private EnemiesCache()
        {
            _enemiesCache.Add("minion", new Minion());
            _enemiesCache.Add("miniBoss", new MiniBoss());
            _enemiesCache.Add("finalBoss", new FinalBoss());
        }

        public Enemy GetEnemy(string type)
        {
            return _enemiesCache.GetValueOrDefault(type).Clone();
        }
    }
}
