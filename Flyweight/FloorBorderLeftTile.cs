﻿using System;

namespace Flyweight
{
    internal class FloorBorderLeftTile : Tile
    {
        public FloorBorderLeftTile(string filePath) : base(filePath) { }

        public override void Draw()
        {
            Console.WriteLine($"Drawing {FilePath} at ({X}, {Y})");
        }
    }
}
