﻿using System;

namespace Flyweight
{
    internal class FloorBorderRightTile : Tile
    {

        public FloorBorderRightTile(string filePath) : base(filePath) { }

        public override void Draw()
        {
            Console.WriteLine($"Drawing {FilePath} at ({X}, {Y})");
        }
    }
}
