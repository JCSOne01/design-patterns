﻿using System;

namespace Flyweight
{
    internal class Program
    {
        private static Map _map;
        private static void Main(string[] args)
        {
            _map = new Map();
            _map.Init();
            _map.Draw();

            Console.ReadLine();
        }
    }
}
