﻿using System;

namespace Flyweight
{
    internal class FloorTile : Tile
    {
        public FloorTile(string filePath) : base(filePath) { }

        public override void Draw()
        {
            Console.WriteLine($"Drawing {FilePath} tile at ({X}, {Y})");
        }
    }
}
