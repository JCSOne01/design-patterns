﻿using System;

namespace Flyweight
{
    internal class SkyTile : Tile
    {
        public SkyTile(string filePath) : base(filePath) { }

        public override void Draw()
        {
            Console.WriteLine($"Drawing {FilePath} at ({X}, {Y})");
        }
    }
}
