﻿using System;
using System.Collections.Generic;

namespace Flyweight
{
    public class TileFactory
    {
        private readonly Dictionary<string, Tile> _tiles = new Dictionary<string, Tile>();

        public Tile GetTile(string type)
        {
            if (_tiles.ContainsKey(type))
            {
                return _tiles[type];
            }

            Console.WriteLine($"Create tile of type {type}");
            switch (type)
            {
                case "floor":
                    _tiles.Add(type, new FloorTile("FloorTile.png"));
                    break;
                case "floorBorderRight":
                    _tiles.Add(type, new FloorBorderRightTile("FloorBorderRightTile.png"));
                    break;
                case "floorBorderLeft":
                    _tiles.Add(type, new FloorBorderLeftTile("FloorBorderLeftTile.png"));
                    break;
                default:
                    _tiles.Add(type, new SkyTile("SkyTile.png"));
                    break;
            }

            return _tiles[type];
        }
    }
}
