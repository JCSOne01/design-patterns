﻿namespace Flyweight
{
    internal class Map
    {
        private int[,] _map;
        private Tile[,] Tiles;
        private TileFactory _tileFactory = new TileFactory();

        public void Init()
        {
            _map = new[,]
            {
                { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 1, 2, 2, 2, 3, 0, 1, 2, 3 }
            };

            Tiles = new Tile[_map.GetLength(0), _map.GetLength(1)];

            for (var i = 0; i < _map.GetLength(0); i++)
            {
                for (var j = 0; j < _map.GetLength(1); j++)
                {
                    switch (_map[i, j])
                    {
                        case 1:
                            Tiles[i, j] = _tileFactory.GetTile("floorBorderLeft");
                            Tiles[i, j].X = j * 128;
                            Tiles[i, j].Y = i * 128;
                            break;
                        case 2:
                            Tiles[i, j] = _tileFactory.GetTile("floor");
                            Tiles[i, j].X = j * 128;
                            Tiles[i, j].Y = i * 128;
                            break;
                        case 3:
                            Tiles[i, j] = _tileFactory.GetTile("floorBorderRight");
                            Tiles[i, j].X = j * 128;
                            Tiles[i, j].Y = i * 128;
                            break;
                        default:
                            Tiles[i, j] = _tileFactory.GetTile("sky");
                            Tiles[i, j].X = j * 128;
                            Tiles[i, j].Y = i * 128;
                            break;
                    }
                }
            }
        }

        public void Draw()
        {
            for (var i = 0; i < _map.GetLength(0); i++)
            {
                for (var j = 0; j < _map.GetLength(1); j++)
                {
                    switch (_map[i, j])
                    {
                        case 1:
                            Tiles[i, j].X = j * 128;
                            Tiles[i, j].Y = i * 128;
                            break;
                        case 2:
                            Tiles[i, j].X = j * 128;
                            Tiles[i, j].Y = i * 128;
                            break;
                        case 3:
                            Tiles[i, j].X = j * 128;
                            Tiles[i, j].Y = i * 128;
                            break;
                        default:
                            Tiles[i, j].X = j * 128;
                            Tiles[i, j].Y = i * 128;
                            break;
                    }
                    Tiles[i, j].Draw();
                }
            }
        }
    }
}
