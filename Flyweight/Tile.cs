﻿namespace Flyweight
{
    public abstract class Tile
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string FilePath { get; set; }

        public Tile(string filePath)
        {
            FilePath = filePath;
        }

        public abstract void Draw();
    }
}
