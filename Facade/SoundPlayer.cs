﻿using System;

namespace Facade
{
    public class SoundPlayer
    {
        public void Init()
        {
            Console.WriteLine("Load sound resources and set a player to play them accordingly");
        }
    }
}