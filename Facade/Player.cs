﻿using System;
using System.Threading.Tasks;

namespace Facade
{
    public class Player
    {
        public bool IsAlive { get; set; }
        public void Init()
        {
            Console.WriteLine("Initializing player and all it's related stuff");
            IsAlive = true;
            Task.Run(() =>
            {
                while (IsAlive)
                {
                    Update();
                    Draw();
                    IsAlive = new Random().NextDouble() >= 0.5;
                }
            });

        }

        private void Update()
        {
            Console.WriteLine("Updating Player");
        }

        private void Draw()
        {
            Console.WriteLine("Drawing player");
        }
    }
}