﻿using System;

namespace Facade
{
    public class GameStateFacade
    {
        private string _state = string.Empty;

        public Map Map { get; private set; }
        public Player Player { get; private set; }
        public SoundPlayer SoundPlayer { get; private set; }

        public GameStateFacade()
        {
            Map = new Map();
            Player = new Player();
            SoundPlayer = new SoundPlayer();
        }

        public void CheckGameState()
        {
            Console.WriteLine($"Game state: {_state}");
        }

        public void InitGame()
        {
            _state = "New Game";
            Map.Init();
            Player.Init();
            SoundPlayer.Init();
        }

        public void ResetGame()
        {
            _state = "Replay";
            Map.Init();
            Player.Init();
            SoundPlayer.Init();
        }
    }
}
