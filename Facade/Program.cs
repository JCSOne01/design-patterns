﻿using System;

namespace Facade
{
    internal class Program
    {
        private static GameStateFacade gameState;

        private static void Main(string[] args)
        {
            gameState = new GameStateFacade();

            gameState.CheckGameState();
            Console.WriteLine();
            gameState.InitGame();
            Console.WriteLine();
            gameState.CheckGameState();
            Console.WriteLine();
            gameState.ResetGame();
            Console.WriteLine();
            gameState.CheckGameState();

            Console.ReadLine();
        }
    }
}
