﻿using System;

namespace Facade
{
    public class Map
    {
        public void Init()
        {
            Console.WriteLine("Loading map from resource files");
        }
    }
}