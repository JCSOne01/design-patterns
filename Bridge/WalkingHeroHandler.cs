﻿using System;

namespace Bridge
{
    internal class WalkingHeroHandler : HeroHandler
    {
        public WalkingHeroHandler(Hero hero) : base(hero)
        {
        }

        public override void GetHealth()
        {
            Console.WriteLine("El heroe aún puede caminar");
        }
    }
}
