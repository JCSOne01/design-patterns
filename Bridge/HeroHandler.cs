﻿namespace Bridge
{
    public abstract class HeroHandler
    {
        private readonly Hero _hero;

        protected HeroHandler(Hero hero)
        {
            _hero = hero;
        }

        public void MoveUp()
        {
            _hero.MoveUp();
        }

        public void MoveDown()
        {
            _hero.MoveDown();
        }

        public void MoveLeft()
        {
            _hero.MoveLeft();
        }

        public void MoveRight()
        {
            _hero.MoveRight();
        }

        public abstract void GetHealth();
    }
}
