﻿using System;

namespace Bridge
{
    public abstract class Hero
    {
        public abstract void MoveUp();
        public abstract void MoveDown();
        public abstract void MoveLeft();
        public abstract void MoveRight();

        public void Update()
        {
            Console.WriteLine("Actualizando el personaje");
        }

        public void Draw()
        {
            Console.WriteLine("Dibujando el personaje");
        }
    }
}
