﻿using System;

namespace Bridge
{
    public class WalkingHero : Hero
    {
        public override void MoveUp()
        {
            Console.WriteLine("Saltando");
        }

        public override void MoveDown()
        {
            Console.WriteLine("Gateando");
        }

        public override void MoveLeft()
        {
            Console.WriteLine("Caminando hacia la izquierda");
        }

        public override void MoveRight()
        {
            Console.WriteLine("Caminando hacia la derecha");
        }
    }
}
