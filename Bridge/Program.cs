﻿using System;

namespace Bridge
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            HeroHandler walkingHeroHandler = new FlyingHeroHandler(new WalkingHero());
            HeroHandler flyingHeroHandler = new FlyingHeroHandler(new FlyingHero());

            Console.WriteLine("Heroe a pie");

            walkingHeroHandler.MoveDown();
            walkingHeroHandler.MoveLeft();
            walkingHeroHandler.GetHealth();

            Console.WriteLine();
            Console.WriteLine("Heroe volando");

            flyingHeroHandler.MoveRight();
            flyingHeroHandler.MoveUp();
            flyingHeroHandler.GetHealth();

            Console.ReadLine();
        }
    }
}
