﻿using System;

namespace Bridge
{
    internal class FlyingHeroHandler : HeroHandler
    {
        public FlyingHeroHandler(Hero hero) : base(hero)
        {
        }

        public override void GetHealth()
        {
            Console.WriteLine("El heroe aún puede caminar");
        }
    }
}
