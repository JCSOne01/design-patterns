﻿using System;

namespace Bridge
{
    public class FlyingHero : Hero
    {
        public override void MoveUp()
        {
            Console.WriteLine("Volando hacia arriba");
        }

        public override void MoveDown()
        {
            Console.WriteLine("Volando hacia abajo");
        }

        public override void MoveLeft()
        {
            Console.WriteLine("Volando hacia la izquierda");
        }

        public override void MoveRight()
        {
            Console.WriteLine("Volando hacia la derecha");
        }
    }
}
