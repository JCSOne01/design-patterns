﻿using System;

namespace Singleton
{
    class Program
    {
        private static MessageService MessageService;

        static void Main(string[] args)
        {
            MessageService.Instance.DisplayMessage("Hola desde un singleton");
            Console.ReadLine();
        }
    }
}
