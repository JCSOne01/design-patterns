﻿using System;

namespace Singleton
{
    public class MessageService
    {
        private static MessageService _instance;
        public static MessageService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new MessageService();
                }

                return _instance;
            }
        }

        private MessageService() { }

        public void DisplayMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}
