﻿namespace Builder
{
    public class HeroBuilder
    {
        private readonly Hero Hero;
        public Hero Sniperhero()
        {
            var hero = new Hero();
            hero.AddWeapon(new Weapon() { Name = "Sniper rifle" });
            hero.AddWeapon(new Weapon() { Name = "Short barrel weapon" });
            return hero;
        }

        public Hero Heavyhero()
        {
            var hero = new Hero();
            hero.AddWeapon(new Weapon() { Name = "Grenade" });
            hero.AddWeapon(new Weapon() { Name = "Bazooka" });
            hero.AddWeapon(new Weapon() { Name = "Rifle" });
            return hero;
        }
    }
}
