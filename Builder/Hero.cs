﻿using System;
using System.Collections.Generic;

namespace Builder
{
    public class Hero
    {
        private List<Weapon> _weapons = new List<Weapon>();

        public void AddWeapon(Weapon weapon)
        {
            _weapons.Add(weapon);
        }

        public void ShowItems()
        {
            foreach (var weapon in _weapons)
            {
                Console.WriteLine(weapon.Name);
            }
        }
    }
}
