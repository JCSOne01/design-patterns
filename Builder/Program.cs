﻿using System;

namespace Builder
{
    internal class Program
    {
        private static HeroBuilder HeroBuilder;
        private static void Main(string[] args)
        {
            var heroBuilder = new HeroBuilder();
            var heavyHero = heroBuilder.Heavyhero();
            Console.WriteLine("Heavy armed hero");
            heavyHero.ShowItems();
            Console.WriteLine();
            var sniperHero = heroBuilder.Sniperhero();
            Console.WriteLine("Heavy armed hero");
            sniperHero.ShowItems();

            Console.ReadLine();
        }
    }
}
