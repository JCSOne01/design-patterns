﻿using System;

namespace Decorator
{
    internal class Program
    {
        private ItemDecorator _decorator;
        private static void Main(string[] args)
        {
            var healthPotion = new Potion("Health 1", "health", 20);
            var revivePotion = new Potion("Phoenix revive 1", "life", 1);

            ItemDecorator consumable = new ConsumableItem(healthPotion);
            consumable.Use();

            Console.WriteLine();

            ItemDecorator combinable = new CombinableItem(healthPotion, revivePotion);
            combinable.Use();

            Console.ReadLine();
        }
    }
}
