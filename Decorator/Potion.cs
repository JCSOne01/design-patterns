﻿using System;

namespace Decorator
{
    public class Potion : Item
    {
        public Potion(string name, string type, int value)
        {
            Name = name;
            Type = type;
            Value = value;
        }

        public override void Use()
        {
            Console.WriteLine($"the {Name} potion adds {Value} to your {Type}");
        }
    }
}
