﻿namespace Decorator
{
    internal class ItemDecorator : Item
    {
        public Item Item { get; private set; }

        public ItemDecorator(Item item)
        {
            Item = item;
        }

        public override void Use()
        {
            Item.Use();
        }
    }
}
