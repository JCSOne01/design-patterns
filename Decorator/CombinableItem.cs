﻿using System;

namespace Decorator
{
    internal class CombinableItem : ItemDecorator
    {
        public Item AddOnItem { get; private set; }
        public CombinableItem(Item item, Item addOnItem) : base(item)
        {
            AddOnItem = addOnItem;
        }

        public void Improve()
        {
            Console.WriteLine($"The {Item.Name} potion now can {AddOnItem.Name} when consumed");
        }

        public override void Use()
        {
            base.Use();
            Improve();
        }
    }
}
