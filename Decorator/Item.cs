﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
    public abstract class Item
    {
        public string Name { get; protected set; }
        public string Type { get; protected set; }
        public int Value { get; protected set; }

        public abstract void Use();
    }
}
