﻿using System;

namespace Decorator
{
    internal class ConsumableItem : ItemDecorator
    {
        public ConsumableItem(Item item) : base(item)
        {
        }

        public void Consume()
        {
            Console.WriteLine($"Consuming {Item.Name}: Your {Item.Type} has gone up by {Item.Value}");
        }

        public override void Use()
        {
            base.Use();

            Consume();
        }
    }
}
